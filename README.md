# To create users that can SSH to AWS EC2 Linux

AWS seems don't allow users to access thier EC2 Linux instance by password.
![nopassword](/img/nopassword.png)

AWS only allow SSH key pair for Linux login. This project will help to guide how to create that :)

[REF: ](https://aws.amazon.com/premiumsupport/knowledge-center/new-user-accounts-linux-instance/)

## Manual user creation
1. Change to root accounts
```sh
sudo su -i
```

2. Create a new user ( Change new_user to the desired new user login name )
```sh
adduser new_user
```

3. Login to the new user
```sh
su - new_user
```
4. Create a baseline for SSH public-private keys
```sh
mkdir .ssh; \
chmod 700 .ssh; \
touch .ssh/authorized_keys; \
chmod 600 .ssh/authorized_keys; \
ssh-keygen -m PEM -N '' <<<$'\n'; \
cat .ssh/id_rsa.pub > .ssh/authorized_keys
```


## Download the first user private key
1. Download the /home/new_user/.ssh/id_rsa

2. If you need to use with Putty you need to convert the id_rsa to ppk by using PuttyGen
![puttygen](/img/puttykeygen.png)

## User the private key at putty to login
1. Set to use the private key at Putty->Connection->SSH-auth

![putty](/img/key.png)

2. Login by entering user name and straight away log in without password.

![nopasswd](/img/login.png)

## Script to create the users
Instead of creating users manually one by one, you can try to use the following script. It will create user

```sh
#!/bin/bash
# Author: Jeffry Johar
# Date: 2/1/2022
# About: To create a user for Linux at AWS that can ssh

USER=$1

adduser $USER
su - $USER -c "mkdir .ssh &&\
 chmod 700 .ssh &&\
 touch .ssh/authorized_keys &&\
 chmod 600 .ssh/authorized_keys &&\
 ssh-keygen -m PEM -N '' <<<$'\n' &&\
 cat .ssh/id_rsa.pub > .ssh/authorized_keys &&\
 find . -exec ls -lah {} +"

```

## For lazy and **unsecured way** to create multiple users
This is definitely not for production use. This is good for setting up a lab for training. 

1. A first user, its ssh keys and its ppk private key has been created. 

2. Copy any user authorized_keys to /home . In this example I'm using user **first_user** . 
```sh
exit
cp /home/first_user/.ssh/authorized_keys /home/authorized_keys
chmod 444 /home/authorized_keys
```
3. Create user by using the following script
```sh
#!/bin/bash
# Author: Jeffry Johar
# Date: 2/1/2022
# About: To create a user for Linux at AWS that can ssh

USER=$1

adduser $USER

#the script only copy existing authorized_keys
su - $USER -c "mkdir .ssh &&\
 chmod 700 .ssh &&\
 touch .ssh/authorized_keys &&\
 chmod 600 .ssh/authorized_keys &&\
 cat /home/authorized_keys > .ssh/authorized_keys&&\
 find . -exec ls -lah {} +"

```

Usage:
```sh
create_user user_two
```

3. Now user_two can use user_one ppk at putty

## **Super** lazy and **unsecured way** to create multiple users
1. Use for loop to loop the lazy script. In this loop I'm creating 3 users named heng, azizi and yuva
```sh
for i in heng azizi yuva; do ./lazy.sh $i; done
```
